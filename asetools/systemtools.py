import os

class cd:
    '''Context manager for switching directories, mimicking shell's "cd".

    Return to original path when leaving the cd context.

    Usage example:

        >>> from asetools.systemtools import cd
        >>>
        >>> path = "path/to/dir"
        >>> with cd(path):
        >>>     continue
    '''

    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)

def read_bgf(fpath, index=-1):
    ''' Read atomic coordinates in bgf file format to ASE Atoms object.

    Parameters:

    fpath : str
        Path to bgf file.

    index : int
        Index of desired image if trajectory is read in.
        Defaults to last image in trajectory.

    Return values:

    atoms : obj
        ASE Atoms object containing the bgf file structure.

    Usage example:

        >>> from ase.visualize import view
        >>> from asetools.systemtools import read_bgf
        >>>
        >>> atoms = read_bgf("path/to/bgf_file")
        >>> view(atoms)
    '''
    from ase import Atoms

    # Initialize trajectory list to hold all images in the file.
    images = []

    # Initialize lists for atomic positions, elemental symbols,
    # and cell dimensions.
    pos = []
    sym = []
    cell = []

    # Read in file.
    with open(fpath, "r") as bgf:
        for line in bgf:
            if "CRYSTX" in line:
               # Read in cell dimensions.
                line = line.split()
                cell = [float(line[1]), float(line[2]), float(line[3])]

            if "HETATM" in line:
                line = line.split()
                sym.append(line[2])
                # ReaxFF-GCMC output formatting is not consistent wrt. the
                # atomic coordinates. Determine at which place the
                # coordinates appear in the split line.
                if "RES" in line:
                    x, y, z = 6, 7, 8
                else:
                    x, y, z = 3, 4, 5
                pos.append([float(line[x]), float(line[y]), float(line[z])])

            if "END" in line:
                # Reached the end of an image. Save image, start new one.
                images.append(Atoms(symbols=sym, cell=cell, positions=pos))
                pos = []
                sym = []
                cell = []

    return images[index]
