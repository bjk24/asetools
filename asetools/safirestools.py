class soluteTagError(Exception):
    pass

def expand_regions(atoms, nqm, natoms, surface=False):
    '''Sets SAFIRES tags for the QM and MM region.

    This function takes an Atoms object, which has to contain a solute,
    for which atoms need to be tagged as tag = 1, and the surrounding
    solvent environment of the same type of solvent molecules all over.

    The solvent molecules are expected to be arranged as [O H H O H H ...]
    in the Atoms object.

    Parameters:

    atoms : obj
        ASE Atoms object that holds the system.

    nqm : int
        Number of molecules supposed to be expanded into the QM region
        (additionally to solute).

    natoms : int
        Number of atoms in every solvent molecule in the QM or MM region.

    surface : bool
        Set to True if the solute is a periodic surface model.

    Return values:

    atoms : obj
        Modified Atoms object with added tags for every atom.

    Usage example:

        >>> from ase.build import molecule
        >>> from asetools.safirestools import find_solute
        >>> from ase.visualize import view
        >>>
        >>> # Set up water box with 9 molecules.
        >>> atoms = molecule("H2O")
        >>> atoms.set_cell((3, 3, 3))
        >>> atoms.center()
        >>> atoms = atoms.repeat((3, 3, 3))
        >>>
        >>> # Assign solute / QM / MM tags.
        >>> atoms = find_solute(atoms, nqm=3, natoms=3)
        >>> print(atoms.get_tags())
        >>> view(atoms[[atom.index for atom in atoms if atom.tag == 1]])
        >>> view(atoms[[atom.index for atom in atoms if atom.tag == 2]])
        >>> view(atoms[[atom.index for atom in atoms if atom.tag == 3]])
    '''

    import numpy as np
    from ase.geometry import find_mic

    # Check if a solute has been defined.
    if not any([1 == atom.tag for atom in atoms]):
        raise soluteTagError(
            " ".join(["Tag for solute required (tag = 1)",
                      "before expansion."]))

    # Calculate COM of solute.
    idx_sol = [atom.index for atom in atoms if atom.tag == 1]
    com_sol = atoms[idx_sol].get_center_of_mass()
    if surface:
        com_sol *= [0, 0, 1]

    # Calculate COMs of all other solvent molecules.
    idx_rest = [atom.index for atom in atoms if atom.index not in idx_sol]
    com_rest = [atoms[idx:idx + natoms].get_center_of_mass()
                for j, idx in enumerate(idx_rest) if j % natoms == 0]
    com_rest = np.array(com_rest)

    if surface:
        com_rest *= [0, 0, 1]

    idx_first = [idx for i, idx in enumerate(idx_rest)
                 if i % natoms == 0]

    # Calculate distances.
    r, d = find_mic((com_rest - com_sol), atoms.cell, atoms.pbc)
    neighbors = sorted(zip(d, idx_first))

    # Set tags, QM = 2 and MM = 3.
    for i, vals in enumerate(neighbors):
        d, idx = vals
        if i < nqm:
            for j in range(natoms):
                atoms[idx + j].tag = 2
        else:
            for j in range(natoms):
                atoms[idx + j].tag = 3

    return atoms

def find_solute(atoms, nqm, natoms):
    '''Find central molecule and make solvent, then expand regions.

    This function is supposed to be used with a model system that only
    consists of one species of molecules. The function will find the
    molecule closest to the center of the cell and define it as the
    solute (tag = 1). It then calls expand_regions() to set tags for
    the QM and MM regions.

    This function will fail if the solute is a different molecule
    from the surrounding solvent molecules.

    Tip: setting nqm to 0 is a valid option if you just need to
    tag the central particle and don't want to expand the QM region.

    Parameters:

    atoms : obj
        ASE Atoms object that holds the system.

    nqm : int
        Number of molecules supposed to be expanded into the QM region.

    natoms : int
        Number of atoms in every solvent molecule in the QM or MM region.

    Return values:

    atoms : obj
        Modified Atoms object with added tags for every atom.

    Usage example:

        >>> from ase.build import molecule
        >>> from asetools.safirestools import expand_regions
        >>> from ase.visualize import view
        >>>
        >>> # Set up water box with 9 molecules.
        >>> atoms = molecule("H2O")
        >>> atoms.set_cell((3, 3, 3))
        >>> atoms.center()
        >>> atoms = atoms.repeat((3, 3, 3))
        >>>
        >>> # Make first water molecule into solute (tag = 1).
        >>> atoms[0].tag = 1
        >>> atoms[1].tag = 1
        >>> atoms[2].tag = 1
        >>>
        >>> # Assign solute / QM / MM tags.
        >>> atoms = expand_regions(atoms, nqm=3, natoms=3)
        >>> print(atoms.get_tags())
        >>> view(atoms[[atom.index for atom in atoms if atom.tag == 1]])
        >>> view(atoms[[atom.index for atom in atoms if atom.tag == 2]])
        >>> view(atoms[[atom.index for atom in atoms if atom.tag == 3]])
    '''

    from ase.geometry import find_mic

    # Calculate list of all centers of mass.
    idx_first = [atom.index for atom in atoms if atom.index % natoms == 0]
    coms = [atoms[idx:idx + natoms].get_center_of_mass() for idx in idx_first]

    # Calculate center of cell and find COM closest to center.
    center = atoms.cell.diagonal() / 2.
    _, d = find_mic((coms - center), atoms.cell, atoms.pbc)
    neighbors = sorted(zip(d, idx_first))

    # Set tag = 1 for molecule closest to center.
    idx_c = neighbors[0][1]
    for i in range(natoms):
        atoms[idx_c + i].tag = 1

    # Expand QM and MM regions around solute.
    return expand_regions(atoms, nqm=nqm, natoms=natoms)