import numpy as np
from ase import Atoms
from ase.geometry import get_distances
from asetools.analysis.coordinationNumbers import generateIdxList

class atomsObjectError(Exception):
    pass

class typeError(Exception):
    pass

class invalidProbe(Exception):
    pass

def radial_density(atoms, probe, binwidth=0.1, integrated=False):
    """Computes the (integrated) radial density of a probe species.

    Parameters:

    atoms : obj
        ASE Atoms object holding the configuration to be analyzed.

    probe : str or int or list or None
        (List of) element symbols and / or atomic indices
        to analyze. Can supply mixed int / str, double-
        counted atoms will be filtered out. Default: None,
        which will perform the analysis on all atoms.

    binwidth : int or float
        The resolution in Angstrom with which radial density
        bins will be evaluated.

    integrated : bool
        Switch between returning the absolute (False) or integrated
        (True) radial atomic density.

    Return values:

    dist : list
        List holding (distance, density) value pairs describing the
        (integrated) radial atomic density of the `probe` species as a
        function of distance from the COM of the structure.

    Usage example:

        >>> import matplotlib.pyplot as plt
        >>> from ase.cluster import Octahedron
        >>> from asetools.analysis.radialDensity import radial_density
        >>>
        >>> atoms = Octahedron("Pt", 10)
        >>>
        >>> # Calculate oxidation states and visualize results.
        >>> dist = radial_density(atoms, probe="Pt")
        >>> x, y = zip(*dist)
        >>> plt.plot(x, y)
        >>> plt.xlabel("$r$ / $\mathrm{\AA}$")
        >>> plt.ylabel("Frequency")
        >>> plt.show()
    """

    # Sanity checks.
    if not isinstance(atoms, Atoms):
        raise atomsObjectError(" ".join(
            ["The configuration supplied to the 'atoms' keyword",
             "argument must be an object of class ase.atoms."]))
    if not isinstance(integrated, bool):
         raise typeError(
            "'integrated' argument must be of type boolean.")
    if not isinstance(binwidth, float) and not isinstance(binwidth, int):
        raise typeError(
            "'binwidth' argument must be of type float or int.")

    # Generate list of atomic indices for which the radial density
    # will be determined.
    idxList = generateIdxList(atoms, probe)

    # Find the center of the structure.
    center = atoms.get_center_of_mass()

    # Determine distance to center for selected atoms. Also, determine
    # the maximum distance from which we will determine the density bins.
    _, d = get_distances(atoms[idxList].get_positions(), center,
                         cell=atoms.cell, pbc=atoms.pbc)
    dmax = max(d)[0] + 1.0

    # Determine number of bins.
    nbins = int(dmax / binwidth)

    # Initialize lists of empty bins.
    # Bins: holds the absolute number of probe atoms within this segment.
    # Intbins: holds the integrated and normalized number of probe atoms
    # within this sphere segment.
    bins = [0] * (nbins + 1)

    # Count number of probe atoms that fall within each bin.
    for atom in atoms[idxList]:
        thisbin = int(d[atom.index] / binwidth)
        if thisbin <= nbins:
            bins[thisbin] += 1

    # If the integrated density is chosen:
    if integrated:
        intbins = [0] * (nbins + 1)

        # Normalize bins.
        # Density = count / sphere volume (up to each bin).
        vol = atoms.get_volume()
        ncnt = len(idxList)
        rho = ncnt / vol  # particle density
        norm = 4 * np.pi * rho * ncnt

        for i, intbin in enumerate(intbins[:-1]):
            intbin = sum(bins[1:i+1])
            r = (i + 1) * binwidth
            rho = ncnt / vol  # particle density
            intbin /= (norm * r * r * binwidth)
            intbins[i+1] = intbin
        bins = intbins

    # Generate list of (distance, density) value pairs.
    r = [binwidth * (i + 1) for i, _ in enumerate(bins)]
    dist = list(zip(r, bins))

    return dist




