import numpy as np

class wavelengthNotImplemented(Exception):
    pass

class typeError(Exception):
    pass

def calcSF(element, thetaRange, wl, coeff):
    """Calculate scattering factors for supplied element pairs"""
    rthetaRange = np.radians(thetaRange)
    a1, b1, a2, b2, a3, b3, a4, b4, c = coeff[element]

    sinDvLa = np.sin(rthetaRange) * np.sin(rthetaRange) / wl / wl
    f = 0.0
    f += a1 * np.exp((-1 * b1 * sinDvLa))
    f += a2 * np.exp((-1 * b2 * sinDvLa))
    f += a3 * np.exp((-1 * b3 * sinDvLa))
    f += a4 * np.exp((-1 * b4 * sinDvLa))
    f += c

    return f

def xrd(atoms, coeff, thetaMin=20, thetaMax=100, thetaInc=0.1,
        I0=0.00001, wavelength="Cu"):
    ''' Calculates a Powder XRD spectrum.

        Uses Debye's equation to calculate the powder XRD
        spectrum of a given structure.

        Parameters:

        atoms : obj
            ASE Atoms object containing the system in question.

        coeff : dict
            A dict of scattering coefficients for every element
            in the structure in the form:
            coeff = {"Symbol": [a1, b1, a2, b2, a3, b3, a4, b4, c]}
            with values according to:
            Cromer, Mann Acta Cryst A24 321-324 (1968).

        thetaMin : float
            Start calculation at this 2theta incidence angle.

        thetaMax : float
            End calculation at this 2theta incidence angle.

        thetaInc : float
            The increment with which the incidence angle 2theta is
            varied (in degrees).

        I0 : float
            Incidence radiation intensity (arbitrary units).
            Default: 0.00001.

        wavelength : str
            Name of the metal whose wavelength is used as a X ray
            source. Options: Cu, Cr, Co, Fe, and Mo.

        Return values:

        spectrum : list
            List containing (theta, intensity) value pairs.

        Usage example:

            >>> import matplotlib.pyplot as plt
            >>> from ase.cluster import Octahedron
            >>> from asetools.analysis.xrd import xrd
            >>>
            >>> # Generate model.
            >>> atoms = Octahedron("Pt", 10)
            >>>
            >>> # Scattering parameters.
            >>> # (Cromer, Mann Acta Cryst A24 321-324 (1968)
            >>> coeff = {'Pt':[36.8102, 13.0747, 11.3323, 2.31421, 1.04422,
            >>>                6.07340, 15.7018, 73.8375, 14.4526]}
            >>>
            >>> # Calculate powder XRD spectrum and visualize results.
            >>> xrd = xrd(atoms, coeff=coeff)
            >>> x, y = zip(*xrd)
            >>> plt.plot(x, y)
            >>> plt.xlabel("$\\2theta$ / °$")
            >>> plt.ylabel("$I(\\theta)$")
            >>> plt.show()
    '''

    # Dictionary of implemented incidence radiation wavelengths.
    wldict = {'Cr': 2.29100, 'Fe': 1.93736, 'Co': 1.79026, 'Cu': 1.54184,
              'Mo': 0.71073}

    # Sanity checks.
    try:
        wl = wldict[wavelength]
    except:
        raise wavelengthNotImplemented(" ".join(
            ["The chosen wavelength {:s}".format(wavelength),
             "is not implemented. Choose from the following:",
             "{:s}".format(str(wldict))]))
    if not isinstance(thetaMin, int) and not isinstance(thetaMin, float):
        raise typeError(" ".join(
            ["Parameter thetaMin must be of type int or float."]))
    if not isinstance(thetaMax, int) and not isinstance(thetaMax, float):
        raise typeError(" ".join(
            ["Parameter thetaMax must be of type int or float."]))
    if not isinstance(thetaInc, int) and not isinstance(thetaInc, float):
        raise typeError(" ".join(
            ["Parameter thetaInc must be of type int or float."]))
    if not isinstance(I0, int) and not isinstance(I0, float):
        raise typeError(" ".join(
            ["Parameter I0 must be of type int or float."]))
    if not isinstance(coeff, dict):
        raise typeError(" ".join(
            ["Parameter coeff must be of type dict with the form",
             "coeff = {'Symbol': [a1, b1, a2, b2, a3, b3, a4, b4, c]",
             "with scattering coefficients according to",
             "Cromer, Mann Acta Cryst A24 321-324 (1968)."]))

    # Calculate distances for all pairs of atoms, avoid self-interaction.
    pairs = []
    for atom1 in atoms[:-1]:
        len_rest = len(atoms[atom1.index:]) - 1
        idx_rest = [atom1.index + i + 1 for i in range(len_rest)]
        d = atoms.get_distances(atom1.index, idx_rest, mic=True)
        symb1 = [atom1.symbol for n in range(len(idx_rest))]
        symb2 = [atom.symbol for atom in atoms[idx_rest]]
        pairs.extend(list(zip(symb1, symb2, d)))

    # Build list of incidence angle values.
    thetaRange = []
    theta = thetaMin / 2
    while theta <= thetaMax / 2:
        thetaRange.append(theta)
        theta += thetaInc

    # Pre-calculate scattering factors for all combinations
    # of elements and incidence angles theta.
    scattering_factors = {}
    for element in set(atoms.get_chemical_symbols()):
        sflist = calcSF(element, thetaRange, wl, coeff)
        scattering_factors.update({element: sflist})

    # Convert thetaRange in radians.
    rthetaRange = np.radians([theta for theta in thetaRange])

    # Calculate diffracted wave vector d.
    dRange = 4. * np.pi * np.sin(rthetaRange) / wl

    # Calculate Debye's equation for all incidence angles.
    Itheta = np.asarray([scattering_factors[pair[0]] *
                         scattering_factors[pair[1]] *
                         np.sin(dRange * pair[2]) / (dRange * pair[2])
                         for pair in pairs])

    # Sum over Itheta of all pairs for each theta.
    Itheta = np.sum(Itheta, axis=0) * I0

    # Combine with thetaRange to get the 2theta vs. I(theta) spectrum.
    spectrum = zip([theta * 2 for theta in thetaRange], Itheta)

    return spectrum