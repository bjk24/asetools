from itertools import combinations
from asetools.analysis.coordinationNumbers import (generateRcDict,
     generateNeighborList, generateIdxList)
from ase import Atoms

class atomsObjectError(Exception):
    pass

class typeError(Exception):
    pass

def findConnected(common, atoms, rcDict):
    """Determine number of bonds between common neighbors."""
    connected = []
    for i, commonAtom1 in enumerate(common[:-1]):
        # commonAtom are atomic indices which relate back to the
        # original atoms list.
        for commonAtom2 in common[i + 1:]:
            dist = atoms.get_distance(commonAtom1, commonAtom2)

            # Determine rc for current pair.
            rc = (rcDict[atoms[commonAtom1].symbol] +
                  rcDict[atoms[commonAtom2].symbol])

            if dist <= rc:
                # Adding tuples of (commonAtom1, commonAtom2)
                # to connected list.
                connected.append((commonAtom1, commonAtom2))
    return connected

def maxBondChain(connected):
    """Calculate the longest connected chain of connected bonds."""
    listMaxBondChain = []
    for i, j in combinations(connected, 2):
        if (i[0]  == j[0] or i[0] == j[1] or i[1] == j[0] or i[1] == j[1]):
            listMaxBondChain.extend([i, j])
        elif (i[0] != j[0] and i[0] != j[1] and i[1] != j[0]
                and i[1] != j[1]):
            listMaxBondChain.append(i)
        else:
            continue
    return len(set(listMaxBondChain))

def cna(atoms, rc=None, factor=1.2, probe=None):
    ''' Calculates a Common Neighbor Analysis.

    Calculates the common neighbor analysis (CNA) according to
    Clarke and Jónsson (Phys. Rev. E, 1993, 47, 3975–3984).
    Parts of this code were contributed by Julian Bessner
    (Ulm University).

    Parameters:

    atoms : obj
        ASE Atoms object containing the system in question.

    rc : float or dict or None
        Cutoff radius for the estimation of nearest-neighbors.
        If float, use this value for all atoms.
        If dict, specify a cutoff for each element, for example
        rc = {'Pt': 2.0, 'O': 1.7}.
        If None, estimate `rc` for each pair of atoms based on their
        covalent radii. This can often fail for unconventional
        and strained structures with unusual bond lengths.

    factor : float
        A multiplication factor that will be applied to rc if
        rc == None. Default: 1.2, i.e. slightly larger than the
        combined atomic radii.

    probe : int or str or list or None
        Which atom or atoms to probe for coordination numbers.
        If int, the atom with this index will be analyzed.
        If str, all atoms with this elemental symbol will be analyzed.
        If None (default), all atoms will be analyzed.
        It is also possible to supply a mixed list of indices and
        atomic symbols. Automatically excludes duplicates if selections
        overlap.

    Return values:

    jklList : dict
        Dict containing atomic indices and their associated
        (j,k,l) triples.

    jklDist : dict
        Dict containing values pairs of (j, k, l) triples
        and the frequency with which they occur.

    Usage example:

        >>> import matplotlib.pyplot as plt
        >>> from ase.cluster import Octahedron
        >>> from asetools.analysis.cna import cna
        >>>
        >>> atoms = Octahedron("Pt", 10)
        >>> jklList, jklDist = cna(atoms)
        >>>
        >>> xticks = []
        >>> y = []
        >>> x = range(len(jklDist))
        >>> for triple, frequency in sorted(jklDist.items()):
        >>>     xticks.append(str(triple))
        >>>     y.append(frequency)
        >>> plt.xticks(x, labels=xticks)
        >>> plt.xlabel("$jkl$")
        >>> plt.ylabel("Frequency")
        >>> plt.bar(x, y)
        >>> plt.show()
    '''

    # Sanity check.
    if not isinstance(atoms, Atoms):
        raise atomsObjectError(" ".join(
            ["The configuration supplied to the 'atoms' keyword",
             "argument must be an object of class ase.atoms."]))

    # Cutoff value in Angstrom based on covalent radii of the elements.
    rcDict = generateRcDict(atoms, rc, factor)

    # Build neighbor list.
    idxList = generateIdxList(atoms, probe)
    neighborList = generateNeighborList(atoms, idxList, rcDict)

    # Calculate (jkl) triples from the results.
    jklList = {}
    for atom1, neighbors1 in neighborList.items():
        for atom2 in neighbors1:

            # Find intersection of neighbor lists. This gives us the j of
            # the (jkl) triple.
            neighbors2 = neighborList[atom2]
            common = list(set(neighbors1) & set(neighbors2))
            j = len(common)

            # Next, find number of bonds between these common neighbors
            # This gives us the k of the (jkl) triple.
            connected = findConnected(common, atoms, rcDict)
            k = len(connected)

            # Find the longest chain of connected common neighbors of the pair.
            # This gives us l of the (jkl) triple.
            l = maxBondChain(connected)

            # Insert to jkl dictionary.
            jklList.update({atom1: (j, k, l)})

    # Calculate frequency of (jkl) triples.
    jklDist = {}
    for _, jkl in jklList.items():
        if jkl != (0, 0, 0):
            if jkl not in jklDist:
                jklDist.update({jkl: 1})
            else:
                jklDist[jkl] += 1

    return jklList, jklDist
