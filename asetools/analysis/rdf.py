import numpy as np
from ase import Atoms
from ase.geometry import get_distances

class idxFormatError(Exception):
    pass

def rdf(images, idx1, idx2, exclude_idx=None, binwidth=0.1, surface=False):
    ''' Calculate radial distribution function.

        The reason that this exists even though ASE already
        has ase.geometry.rdf.get_rdf() is that the latter
        only accepts a single Atoms object. Also, this
        implementation can do g(z) for surface models.

        Parameters:

        images : obj
            ASE Atoms object containing the system or
            ASE Trajectory object containing a series of images.
            If a Trajectory is supplied, all images need to be
            of the same length and have the same atomic indexing.

        idx1, idx2 : str or list of int
            Atoms to use for the RDF calculation. Can be string
            (name of elements) or list of strings, or can be list
            of atomic indices (int) referring to the Atoms object.

        exclude_idx : int or list of int
            Atomic indices to be ignored used to refine selection
            made by idx1 and idx2.

        bindwidth : float
            The distance increments (in Angstrom) at which sphere
            segments will be evaluated.

        surface : bool
            If True, returns the g(z) instead of the g(r). Chose
            idx1 or idx2 appropriately to contain an atom within
            the surface that the g(z) should be in reference to.

        Return values:

        g_r : list of tuples
            Returns list containing the g(r) as (r, g(r))
            tuples.

        Usage example:

            >>> import matplotlib.pyplot as plt
            >>> from ase.build import molecule
            >>> from asetools.analysis.rdf import rdf
            >>>
            >>> # Set up periodic water cell.
            >>> atoms = molecule("H2O")
            >>> atoms.cell = [3, 3, 3]
            >>> atoms.center()
            >>> atoms = atoms.repeat((10, 10, 10))
            >>>
            >>> # Calculate RDF and visualize results.
            >>> g_r = rdf(atoms, "O", "O")
            >>> x, y = zip(*g_r)
            >>> plt.plot(x, y)
            >>> plt.xlabel("$r$ / $\mathrm{\AA}$")
            >>> plt.ylabel("$g(r)$")
            >>> plt.show()
    '''

    # Pick first image in case of trajectory to do setup with.
    if isinstance(images, Atoms):
        # If a singular Atoms object is passed, put into list
        # for consistency with the code below.
        images = [images]
    atoms = images[0]

    # If there is no cell, we need to add one based on the
    # dimensions of the system since we need a reference volume.
    if not atoms.cell:
        print("No cell vectors found, using model dimensions to",
              "estimate them to get the normalization volume.")
        # Translate so that all positions are positive.
        pos = atoms.get_positions()
        shiftmin = np.amin(atoms.get_positions(), axis=0)
        atoms.set_positions(atoms.get_positions() - shiftmin)

        # Determine largest x, y, z coordinates, make that the cell.
        atoms.cell = np.amax(atoms.get_positions(), axis=0)
    vol = atoms.get_volume()

    if exclude_idx != None:
        # Make exclude_idx into list if it isn't already.
        if not isinstance(exclude_idx, list):
            exclude_idx = [exclude_idx]
        # Make sure exclude_idx only contains int.
        if not all([isinstance(ei, int) for ei in exclude_idx]):
            raise idxFormatError(" ".join(
                ["Parameter exclude_idx must be of type int",
                 "or a list of int."]))

    # Assert that idx1/2 is either string or list of int.
    # If str (element symbol): expand to list of indices.
    indices = [[],[]]
    allElements = set(atoms.get_chemical_symbols())
    for i, idx in enumerate([idx1, idx2]):
        # Always convert to list for consistency.
        if not isinstance(idx, list):
            idx = [idx]
        # Expand index list based on int (index) or str (element).
        for id in idx:
            if isinstance(id, int):
                indices[i].append(id)
            elif isinstance(id, str):
                if not id in allElements:
                    raise idxFormatError(
                        " ".join(
                            ["The element symbol '{:s}'".format(id),
                             "you provided does not exist",
                             "in the Atoms object."]))
                else:
                    indices[i].extend([atom.index for atom in atoms
                                       if atom.symbol == id])
            else:
                raise idxFormatError(
                    " ".join(["You must either pass a string (elemental",
                              "symbol), list of str, int (atomic index),",
                              "list of int, or mixed int/str list",
                              "to this function."]))

    # Because a mixed list of indices and symbols can be provided,
    # there can be duplicates in the selection -> remove.
    # If exclude_idx was defined, we remove those from the lists.
    # Finally, we're also sanity-checking the selection here.
    for i, idx in enumerate(indices):
        if idx:
            if exclude_idx != None:
                idx = [id for id in idx if id not in exclude_idx]
            indices[i] = list(set(idx))
        else:
            raise idxFormatError(" ".join(["Index selection is empty.",
                                       "There is an issue with the",
                                       "choice of idx1 and idx2."]))
    idx1, idx2 = indices

    # Determine the number of unique atoms to be evaluated.
    ncnt = len(set(idx1 + idx2))

    # Determine number of bins from largest cell dimension and
    # binwidth parameter. Determine number of images in trajectory.
    cell = atoms.cell

    if surface:
        # Adjust cell z parameter internally for better
        # viewing (cell might be much higher than maximum
        # distances because of vacuum slab).
        pos = atoms.get_positions()
        maxz = np.max(pos)
        cell[2][2] = maxz + 10.0
        cellhmax = cell[2][2]
    else:
        cellh = cell / 2.0
        cellhmax = np.amax(cellh)
    nbins = int(cellhmax / binwidth)
    nimages = len(images)

    # Set up collection bins.
    bins = np.array([0] * (nbins + 1))

    # Determine summation increment dr.
    dr = cellhmax / nbins

    # Create mask to remove self-interaction
    # (distance between the same atoms).
    combinations = []
    for id1 in idx1:
        combinations.extend([(id1, id2) for id2 in idx2])
    mask = np.asarray([False if pair[0] == pair[1] else True
                       for pair in combinations])

    # For each image in images, calculate pair distances
    # and assign to bins.
    for atoms in images:
        pos = atoms.get_positions()

        # In case of surface, reduce to g(z).
        if surface:
            pos *= [0,0,1]

        # Calculate all pair distances.
        _, dist = get_distances(pos[idx1], pos[idx2],
                                cell=cell, pbc=atoms.pbc)
        dist_flat = dist.flatten()

        # Remove self-interaction.
        dist_dr = dist_flat[mask] / dr

        dist_dr_int = dist_dr.astype(int)
        bins += [np.count_nonzero(dist_dr_int == i + 1)
                 for i in range(nbins + 1)]

    # Normalize the g(r) and return list of (r, g(r) tuples.
    rho = ncnt / vol * nimages
    norm = 4 * np.pi * rho * ncnt
    g_r = []
    for i, value in enumerate(bins):
        rr  = (i - 0.5 ) * dr
        value /= (norm * rr * rr * dr)
        g_r.append((rr, value))

    return g_r
