import numpy as np
from ase import Atoms
from ase.geometry import get_distances
from asetools.analysis.coordinationNumbers import (generateIdxList,
    generateRcDict, generateNeighborList)

class atomsObjectError(Exception):
    pass

class typeError(Exception):
    pass

class invalidProbe(Exception):
    pass

def oxidation_states(atoms, probe=None, rc=None, factor=1.2,
                     binwidth=0.2, nstates=4):
    """Simple, coordination-based oxidation state analysis.

    Adapted from: J. Phys. Chem. C 2015, 119, 17876-17886.

    This script counts the number of homogeneous and heterogeneous
    bonding partners for each atom. The more non-identical atoms
    a particular atom is surrounded by, the higher we will assume
    its oxidation state. The resulting counts are presented as
    a radial number density that originates from the center of
    the structure.

    Example: Pt in a bulk fcc Pt cell has
    only Pt bonding partners and will therefore be deemed to be
    neutral. Pt in a alpha-PtO2 cell is completely surrounded by
    O bonding partners and will be assigned a high oxidation
    state.

    This analysis is qualitative in nature and cannot replace
    an actual partial charge / oxidation state analysis based
    on an electronic structure calculation.

    Note: in case of metallic nanoparticles for example, the
    outermost atoms will be counted as neutral oxidation
    state even though they are not fully saturated because
    this model considers the relative number of non-identical
    nearest neighbors to estimate the oxidation state and not
    whatever would be the ideal number of bonds in fully
    saturated bulk system.

    Parameters:

    atoms : obj
        ASE atoms object holding the atomic configuration
        to be analyzed.

    probe : str or int or list or None
        (List of) element symbols and / or atomic indices
        to analyze. Can supply mixed int / str, double-
        counted atoms will be filtered out. Default: None,
        which will perform the analysis on all atoms.

    rc : float or dict or None
        Cutoff radius for the estimation of nearest-neighbors.
        If float, use this value for all atoms.
        If dict, specify a cutoff for each element, for example
        rc = {'Pt': 2.0, 'O': 1.7}.
        If None, estimate `rc` for each pair of atoms based on their
        covalent radii. This can often fail for unconventional
        and strained structures with unusual bond lengths.

    factor : float
        A multiplication factor that will be applied to rc if
        rc == None. Default: 1.2, i.e. slightly larger than the
        combined atomic radii.

    binwidth : int or float
        The resolution in Angstrom with which radial density
        bins will be evaluated.

    nstates : int >= 1
        The number of oxidation states that the analyis will
        try to differentiate. Default: 4. The default setting
        will assing states 0 to 3 to atoms that are surrounded
        by > 75%, 75-51%, 50-26%, and <= 25% of the same element
        of atom.

    Return values:

    binList : list of lists
        A list containing sublists for each oxidation state
        composed of (r, frequency) value pairs, where r is the
        distance from the COM of the structure and frequency is
        the count of how many atoms at this distance show the
        oxidation state in question.

    Usage example:

            >>> from numpy.random import randint
            >>> import matplotlib.pyplot as plt
            >>> from ase.cluster import Octahedron
            >>> from asetools.analysis.oxidationStates import oxidation_states
            >>>
            >>> atoms = Octahedron("Pt", 10)
            >>>
            >>> # Randomly change some Pt atoms to O.
            >>> for atom in atoms:
            >>>     if randint(0,3) == 0:
            >>>     atom.symbol = "O"
            >>>
            >>> # Calculate oxidation states and visualize results.
            >>> ostates = oxidation_states(atoms, probe="Pt", rc=3.0)
            >>> for i, state in enumerate(ostates):
            >>>     r, freq = zip(*state)
            >>>     plt.plot(r, freq, label="state {:d}".format(i))
            >>> plt.legend()
            >>> plt.show()
    """

    # Sanity check.
    if not isinstance(atoms, Atoms):
        raise atomsObjectError(" ".join(
            ["The configuration supplied to the 'atoms' keyword",
             "argument must be an object of class ase.atoms."]))
    if not isinstance(binwidth, float) and not isinstance(binwidth, int):
        raise typeError(
            "'binwidth' argument must be of type float or int.")
    if not isinstance(nstates, int):
        raise typeError(
            "'nstates' argument must be of type int.")

    # It is convenient to assume the center of mass as
    # the center of the following radial density analysis.
    center = atoms.get_center_of_mass()

    # Find the atom with the largest distance from the center.
    _, d = get_distances(atoms.get_positions(), center,
                         cell=atoms.cell, pbc=atoms.pbc)
    dmax = max(d)[0]

    # Determine number of bins.
    nbins = int(dmax / binwidth)

    # Initialize list-of-lists containing
    # distributions based on nstates.
    binList = [[[binwidth * i, 0] for i in range(nbins + 1)]
                for n in range(nstates)]

    # Make list of atomic indices to be analyzed.
    idxList = generateIdxList(atoms, probe)

    # Generate dict of cutoff radii.
    rcDict = generateRcDict(atoms, rc, factor)

    # Make list of neighbors for each atom in idxList.
    neighborList = generateNeighborList(atoms, idxList, rcDict)

    # Calculate percentage of nearest neighbors of the
    # same element for each atom in idxList. Count in
    # corresponding bin in binList based on distance
    # from the center.
    for idx, neighbors in neighborList.items():
        nsame = np.count_nonzero(
            np.asarray(atoms[neighbors].get_chemical_symbols()) ==
                       atoms[idx].symbol)
        state = round(nsame / len(neighbors) * nstates) - 1

        rbin = round(np.linalg.norm(atoms[idx].position - center)
                     / binwidth)
        binList[state][rbin][1] += 1

    # Currently the states are ordered least to most number of
    # same-element partners. This is unintuitive, we want to
    # return in the sequence of low oxidation state to high
    # oxidation state.
    binList = list(reversed(binList))

    return binList