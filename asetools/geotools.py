class excludeListMalformed(Exception):
    pass

class cellShapeError(Exception):
    pass

def assert_kwargs(exclude_idx, exclude_sym):
    '''Assert if atoms and exclude args are usable.'''

    # Always convert exclude argument to list for easier handling.
    if exclude_idx and not isinstance(exclude_idx, list):
        exclude_idx = [exclude_idx]
    if exclude_sym and not isinstance(exclude_sym, list):
        exclude_sym = [exclude_sym]

    # Assert that exclude_idx is a list of atomic indices if given.
    if exclude_idx:
        if not all([isinstance(idx, int) for idx in exclude_idx]):
            raise excludeListMalformed(
                " ".join(['"exclude_idx" list argument appears to contain',
                         'something else besides atomic index integers.']))
    else:
        # Initialize as empty.
        exclude_idx = []

    # Assert that exclude_sym is a list of element symbols if given.
    if exclude_sym:
        if not all([isinstance(sym, str) for sym in exclude_sym]):
            raise excludeListMalformed(
                " ".join(['"exclude_sym" list argument appears to contain',
                         'something else besides atomic element symbol',
                         'strings.']))
    else:
        # Initialize as empty.
        exclude_sym = []

    return exclude_idx, exclude_sym

def make_rattle_tuples(atoms, natoms, exclude_idx=None, exclude_sym=None):
    '''Create tuples of atomic indices for the FixBondLengths class.

    Example: to use FixBondLengths with water molecules, we have to
    pass a list of tuples corresponding to [(O, H1), (O, H2), (H1, H2)]
    as parameter, where "O" and "H1" are indices of the atoms in the
    atoms object. This function is a generater for this tuple list.

    IMPORTANT: we assume that the atoms object is ordered like
    Atoms(mol1, mol2, mol3, ...) where each mol has natoms atoms.

    Parameters:

    atoms : obj
        ASE atoms object which includes the to-be-constrained molecules
        in a fixed sequence. Example for H2O: [O H H O H H O H H ...].

    natoms : int
        Number of atoms in each molecule that will be constrained.

    exclude_idx : list of int
        List of atomic indices to be excluded from the tuple generator.

    exclude_sym : list of str
        List of elemental symbols to be excluded from the tuple
        generator.

    Return values:

    tuple_list : list of tuples
        List including tuples of atomic indices to be passed to
        the FixBondLengths class.

    Usage example:

        >>> from ase.build import molecule
        >>> from asetools.geotools import make_rattle_tuples
        >>> from ase.constraints import FixBondLengths
        >>>
        >>> atoms = molecule("H2O")
        >>> tuples = make_rattle_tuples(atoms, natoms=3)
        >>> atoms.constraints = FixBondLengths(tuples)
        >>> print(tuples)
    '''

    # Assert usability of Atoms object and initialize exclude list.
    exclude_idx, exclude_sym = assert_kwargs(exclude_idx, exclude_sym)

    # Make list of indices based on exclude filters.
    idx_fltr = [atom.index for atom in atoms if atom.index not in exclude_idx
                and atom.symbol not in exclude_sym]

    # Check if filtered index list agrees with natoms input parameter.
    assert (len(idx_fltr) % natoms == 0), \
        'Length of the filtered Atoms object is not compatible with ' \
        'provided natoms keyword parameter!'

    # Generate tuples for all permutations of atomic indices in each
    # molecule.
    tuple_list = ([((i + j), (i + (j + 1) % natoms))
                   for i in idx_fltr[::natoms]
                   for j in [0, 1, 2]])

    return tuple_list

def molwrap(atoms, natoms, exclude_idx=None, exclude_sym=None,
            pretty_translation=False):
    '''Wrap an atoms object so that molecules are unbroken over PBCs.

    IMPORTANT: we assume that the atoms object is ordered like
    Atoms(mol1, mol2, mol3, ...) where each mol has natoms atoms.

    Parameters:

    atoms : obj
        ASE atoms object.

    natoms : int
        Number of atoms in each molecule that will be adjusted.

    exclude_idx : list of int
        List of atomic indices to be excluded from the adjustment.

    exclude_sym : list of str
        List of elemental symbols to be excluded from the adjustment.

    pretty_translation : bool
        Passed as argument of the same name to ase.Atoms.wrap().

    Return values:

    atoms : obj
        Wrapped ASE atoms object.

    Usage example:

        >>> from ase.build import molecule
        >>> from ase.visualize import view
        >>> from asetools.geotools import molwrap
        >>>
        >>> # Place watermolecule outside box and visualize.
        >>> atoms = molecule("H2O")
        >>> atoms.cell = [5, 5, 5]
        >>> atoms.pbc = True
        >>> atoms.translate([6, 5, 5])
        >>> view(atoms)
        >>>
        >>> # Visualize behavior of regular wrap.
        >>> # (Water molecule broken over PBCs.)
        >>> brokenatoms = atoms.copy()
        >>> brokenatoms.wrap()
        >>> view(brokenatoms)
        >>>
        >>> # Visualize behavior of molwrap.
        >>> # (Molecule remains intact.)
        >>> atoms = molwrap(atoms, natoms=3)
        >>> view(atoms)
    '''

    # Assert usability of Atoms object and initialize exclude list.
    exclude_idx, exclude_sym = assert_kwargs(exclude_idx, exclude_sym)

    # Make list of indices based on exclude filters.
    idx_fltr = [atom.index for atom in atoms if atom.index not in exclude_idx
                and atom.symbol not in exclude_sym]

    # Wrap atoms object.
    atoms.wrap(pretty_translation=pretty_translation)

    # Loop through filtered atoms list in natoms slices and adjust positions
    # by the difference between the MIC and non-MIC distance vector.
    for mol in idx_fltr[::natoms]:
        for i in range(natoms - 1):
            current = mol + i + 1
            d = atoms.get_distance(mol, current, mic=False, vector=True)
            d_mic = atoms.get_distance(mol, current, mic=True, vector=True)
            atoms[current].position += d_mic - d

    return atoms

def vacuum_padding(atoms, vacuum=0, pretty_translation=False):
    '''Change cell to have the desired amount of vacuum in each direction.

    This function will first reduce the cell to the smalles possible
    cell based on the coordinates of the atoms. Afterwards, a padding
    defined by the vacuum parameter is applied along each non-periodic
    axis.

    Parameters:

    atoms : obj
        ASE Atoms object holding the system.

    vacuum : int or list
        Desired amount of vacuum (in Ang) for each axis. Can be integer
        or list. If integer, atoms.pbc are used to decide whether or
        not the vacuum is applied (only in non-periodic direction).

    pretty_translation : bool
        Passed as argument of the same name to ase.Atoms.wrap().

    Return values:

    atoms : obj
        ASE atoms object where the cell has been changed to include
        the desired vacuum padding.

    Usage example:

        >>> from ase.build import molecule
        >>> from ase.visualize import view
        >>> from asetools.geotools import vacuum_padding
        >>>
        >>> # Build water molecule and visualize too-large cell.
        >>> atoms = molecule("H2O")
        >>> atoms.cell = [20, 20, 20]
        >>> view(atoms)
        >>>
        >>> # Shrink cell to molecule size + 5 A safety padding.
        >>> atoms = vacuum_padding(atoms, vacuum=5)
        >>> view(atoms)
    '''

    from warnings import warn

    # Wrap system without splitting molecules.
    atoms.wrap(pretty_translation=pretty_translation)

    # Assert that cell is rectangular.
    if not (0. == sum(atoms.cell.sum(axis=0)
                      - [atoms.cell[i][i] for i in range(3)])):
        raise cellShapeError(
            'Only works for rectangular cells.')

    # Shift coordinates to (0, 0, 0) origin.
    pos = atoms.get_positions()
    mincell = pos.min(axis=0)
    pos -= mincell
    atoms.set_positions(pos)

    # Find largest axis value.
    posmax = pos.max(axis=0)
    if all(atoms.pbc):
        atoms.cell = posmax
    else:
        for i in range(3):
            if not atoms.pbc[i]:
                atoms.cell[i][i] = posmax[i]

    if not isinstance(vacuum, list):
        vacuum = [vacuum, vacuum, vacuum]
    else:
        if not all(atoms.pbc) or not any(atoms.pbc):
            warn("Vacuum list specified but the system appears"
                 " to have 1D or 2D periodicity. We will only"
                 " use the corresponding value for any"
                 " non-periodic axes!")

    # Pad cell with vacuum according to PBC or explicit list.
    if all(atoms.pbc):
        for i in range(3):
            atoms.cell[i][i] += vacuum[i]
    else:
        for i in range(3):
            if not atoms.pbc[i]:
                atoms.cell[i][i] += vacuum[i]
    atoms.center()

    return atoms
