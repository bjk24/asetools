import os
from ase.io.bader import attach_charges
from ase.data import atomic_numbers
from ase.io import read
from ase.io.vasp import write_vasp
from ase.calculators.vasp import Vasp

class pathError(Exception):
    pass

class environmentError(Exception):
    pass

class typeError(Exception):
    pass

def clean_poscar(infile, outfile, force=False, xc="PBE"):
    """Clean POSCAR file (to vasp5 format) and generate POTCAR.

    Warning: if the same filename is given for infile and outfile,
    infile will be overwritten!

    Parameters:

    infile, outfile : str
        Paths for the file to be read (infile) and the new, cleaned
        file to be written (outfile).

    force : bool
        Set True to force overwriting of infile without dialog.

    xc : str
        Which POTPAW files should be used to generate the POTCAR.
        Default: PBE (usually fine).

    Usage example:

         >>> from asetools.vasptools import clean_poscar
         >>> clean_poscar("CONTCAR", "POSCAR")
    """
    # Sanity check.
    if not os.getenv('VASP_PP_PATH'):
        raise environmentError(" ".join(
            ["Environment variable $VASP_PP_PATH",
             "is not set but is required for operation."]))
    if not isinstance(force, bool):
        raise TypeError("Parameter 'force' must be of type boolean.")
    if not isinstance(xc, str):
        raise TypeError("Parameter 'xc' must be of type str.")

    # Unless force == True, prompt user before overwriting infile.
    if infile == outfile and not force:
        answer = input(" ".join(
            ["This action will overwrite '{:s}'.".format(infile),
             "Continue anyways? (y/n)"]))

        if answer.lower() not in ["y","yes"]:
            raise pathError(" ".join(
                ["Exiting (user choice to not overwrite"
                 "'{:s}'.".format(infile)]))

    # Read in file, write out in clean vasp5 format, generate POTCAR.
    a = read(infile)
    calc = Vasp(xc=xc)
    calc.initialize(a)
    calc.write_potcar()
    write_vasp(outfile, calc.atoms_sorted,
               symbol_count=calc.symbol_count, vasp5=True)

    return

def visualize_bader(atoms, acf="ACF.dat", potcar="POTCAR"):
    """Visualize Bader charges.

    Reads Bader charges from ACF.dat and corrects
    the charge values by the valence charge value
    in the corresponding POTCAR file for each element.

    Important: sequence of atoms in `atoms`and in `acf` file
    need to be identical!

    Parameters:

    atoms : obj
        ASE atoms object holding the structure.

    acf : str
        Complete path to the file holding the Bader
        charges, by default "ACF.dat" in the same
        folder where this script is executed.

    potcar : str
        Complete path to the POTCAR file used for
        the calculation, by default "POTCAR" in the
        same folder where this script is executed.

    Return values:

    atoms : obj
        ASE atoms object holding the structure with
        attached corrected Bader charge values for
        each atom.

    Usage example:

        >>> from ase.io import read
        >>> from asetools.vasptools import visualize_bader
        >>>
        >>> atoms = read("CONTCAR")
        >>> atoms = visualize_bader(atoms)
        >>> ase.visualize.view(atoms)
    """
    # Sanity checks.
    if not isinstance(acf, str):
        raise TypeError("Parameter 'acf' must be of type str.")
    if not isinstance(potcar, str):
        raise TypeError("Parameter 'potcar' must be of type str.")
    if not os.path.exists(acf):
        raise pathError("Path '{:s}' does not exist.".format(acf))
    if not os.path.exists(potcar):
        try:
            calc = Vasp(xc="PBE")
            calc.initialize(atoms)
            calc.write_potcar()
        except:
            raise pathError(" ".join(
                ["Path '{:s}' does not exist".format(potcar),
                 "and we were unable to automatically generate",
                 "a POTCAR file from the given Atoms object",
                 "(most likely $VASP_PP_PATH is not set."]))

    # Read out "charges" column from ACF.dat and
    # associate them with each atom.
    attach_charges(atoms, acf)

    # Define a lookup dictionary with valence
    # electron count from POTCAR.
    # Change / extend this dictionary as needed.
    valencies = {}
    with open(potcar, "r") as pfile:
        data = pfile.readlines()
    for i, line in enumerate(data):
        if "PAW_PBE" in line and not "TITEL" in line:
            element = line.split()[1]
            element = element.split("_")[0]
            val = float(data[i+1].split()[0])
            valencies.update({element: val})

    # Correct for atom number and valency from POTCAR.
    for atom in atoms:
        atom.charge -= atomic_numbers[atom.symbol]
        atom.charge += valencies[atom.symbol]

    return atoms
