def assert_kwargs(atoms, exclude_idx, exclude_sym):
    '''Assert if atoms and exclude args are usable.'''

    # Assert that Atoms object contains water molecules at all.
    assert (any("O" == atom.symbol for atom in atoms) and
            any("H" == atom.symbol for atom in atoms)), \
        'Cannot detect any water molecules in the provided atoms object.'

    # Always convert exclude argument to list for easier handling.
    if exclude_idx and not isinstance(exclude_idx, list):
        exclude_idx = [exclude_idx]
    if exclude_sym and not isinstance(exclude_sym, list):
        exclude_sym = [exclude_sym]

    # Assert that exclude_idx is a list of atomic indices if given.
    if exclude_idx:
        assert all(isinstance(idx, int) for idx in exclude_idx), \
            '"exclude_idx" list argument appears to contain something else \
            besides atomic index integers.'
    else:
        # Initialize as empty.
        exclude_idx = []

    # Assert that exclude_sym is a list of element symbols if given.
    if exclude_sym:
        assert all(isinstance(sym, str) for sym in exclude_sym), \
            '"exclude_sym" list argument appears to contain something else \
            besides atomic element symbol strings.'
    else:
        # Initialize as empty.
        exclude_sym = []

    return exclude_idx, exclude_sym

def find_neighbors(atoms, exclude_idx):
    '''Build [O_1 H_11 H_12 O_2 H_21 H_22 ...] list of H2O molecules.'''

    allHidx = [atom.index for atom in atoms if atom.symbol == "H"
               and atom.index not in exclude_idx]
    idxlist = []
    for atom in atoms:
        if atom.symbol == "O" and atom.index not in exclude_idx:
            d = atoms.get_distances(atom.index, allHidx, mic=True)
            combined = sorted(zip(d, allHidx))
            idxlist.append(atom.index)
            idxlist.append(combined[0][1])
            idxlist.append(combined[1][1])

    return idxlist

def rearrange_atoms(atoms, prepend=False, exclude_idx=None, exclude_sym=None):
    '''Rearrange H2O configuration to comply with [O H H O H H O H H ...].

    WARNING: This function strips all properties from the Atoms object
    except for atoms.cell and atoms.pbc. Use this before you start
    attaching calculators, constraints, *etc.* to the atoms.

    Parameters:

    atoms : obj
        ASE Atoms object which contains water molecules (and potentially
        other atoms) in any ordering.

    prepend : bool
        If the Atoms object contains elements other than O and H, this
        switch can be used to decide if the other elements should come
        before (True) or after (False) the sequence of water molecules.
        Default: False (final structure: [X Y Z O H H O H H O H H ...]).

    exclude_idx : list of int
        List of atomic indices corresponding to the original Atoms
        object which should be excluded from adjustment.

    exclude_sym : list of str
        List of elemental symbols corresponding to the original Atoms
        object which should be excluded from adjustment.

    Return values:

    atoms : obj
        Rearranged ASE Atoms object where the ordering of water molecules
        complies with the order [O H H O H H O H H ...].

    Usage example:

        >>> from ase import Atoms
        >>> from asetools.watertools import rearrange_atoms
        >>>
        >>> atoms = Atoms("HOH")
        >>> print("before: ", atoms.get_chemical_symbols())
        >>> atoms = rearrange_atoms(atoms)
        >>> print("after: ", atoms.get_chemical_symbols())
    '''

    from warnings import warn
    from ase import Atoms

    # Assert usability of Atoms object and initialize exclude lists.
    exclude_idx, exclude_sym = assert_kwargs(atoms, exclude_idx, exclude_sym)

    # Warn user if Atoms object has constraints on it.
    if atoms.constraints:
        warn("This function removes all attached constraints.")

    # Warn user if Atoms object has calculator on it.
    if atoms.calc:
        warn("This function removes any attached calculator.")

    # Find nearest H atoms for each O atom to define H2O molecules.
    idxlist = find_neighbors(atoms, exclude_idx)

    # Make new Atoms object, inherit cell and pbc from parent Atoms.
    newatoms = Atoms()
    newatoms.cell = atoms.cell
    newatoms.pbc = atoms.pbc

    # Merge water and non-water Atoms into newatoms according to
    # sorting given by prepend kwarg.
    if prepend == True:
        for idx in idxlist:
            newatoms += atoms[idx]
    for atom in atoms:
        if atom.symbol in exclude_sym or atom.index in exclude_idx:
            newatoms += atom
    if prepend == False:
        for idx in idxlist:
            newatoms += atoms[idx]

    return newatoms

def adjust_distances(atoms, d_oh=0.9572, d_hh=1.524, threshold=1e-7,
                     maxiters=200, exclude_idx=None, exclude_sym=None):
    '''Recursively adjust O-H1, O-H2, H1-H2 bond legths to specified value.

    Parameters:

    atoms : obj
        ASE Atoms object containing water molecules. Atomic ordering must
        comply with [O H H O H H O H H O H H ...].

    d_oh : float
        Adjust O-H1 and O-H2 distances in each H2O to this value (in Ang).
        Default value: experimental O-H distance.

    d_hh : float
        Adjust H-H distance in each H2O to this value (in Ang).
        Default value: experimental H-H distance.

    threshold : float
        The adjustment loop will be repeated until the difference
        between all bond distances and the target d_oh and d_hh
        values are below the threshold. Default is 1e-7 to play
        nicely with RATTLE constraints, for which the default
        threshold value is the same in
        `gpaw.utilitieswatermodel.FixBondLengthsWaterModel`.

    maxiters : int
        Maximum number of tries to adjust bond lengths so that the
        difference between all bond lengths and the target values
        is below the threshold. Default is a very safe 200, if
        convergence isn't reached by then, there is most likely
        an issue with the structure.

    exclude_idx : list of int
        List of atomic indices corresponding to the original Atoms
        object which should be excluded from adjustment.

    exclude_sym : list of str
        List of elemental symbols corresponding to the original Atoms
        object which should be excluded from adjustment.

    Return values:

    atoms : obj
        ASE Atoms object containing water molecules for which O-H
        and H-H distances have been changed according to d_oh and d_hh.

    Usage example:

        >>> from ase import Atoms
        >>> from asetools.watertools import adjust_distances
        >>>
        >>> pos = [[ 0.0,  0.8, -0.5],
        >>>        [ 0.0,  0.0,  0.1],
        >>>        [ 0.0, -0.8, -0.5]]
        >>> atoms = Atoms("OHH", positions=pos)
        >>> print("before: ", atoms.get_all_distances())
        >>> atoms = adjust_distances(atoms)
        >>> print("after: ", atoms.get_all_distances())
    '''

    # Assert usability of Atoms object and initialize exclude lists.
    exclude_idx, exclude_sym = assert_kwargs(atoms, exclude_idx, exclude_sym)

    # Find nearest H atoms for each O atom to define H2O molecules.
    idxlist = find_neighbors(atoms, exclude_idx)

    # Adjust atomic positions for each water molecule in a
    # round-robin fashion.
    for i, idxO in enumerate(idxlist[0::3]):
        idxH1 = idxlist[i * 3 + 1]
        idxH2 = idxlist[i * 3 + 2]
        cur_d_oh1, cur_d_oh2 = atoms.get_distances(idxO, [idxH1, idxH2])
        cur_d_hh = atoms.get_distance(idxH1, idxH2)
        iter = 0
        while (abs(cur_d_oh1 - d_oh) >= threshold or
               abs(cur_d_oh2 - d_oh) >= threshold or
               abs(cur_d_hh - d_hh) > threshold):
            iter += 1
            if iter >= maxiters:
                raise SystemExit("adjust_distances did not converge\
                                  after {:d} iterations for atoms\
                                  {:d}, {:d}, and {:d}. Check your\
                                  system or increase maxiters.".format(
                                  idxO, idxH1, idxH2, maxiters))
            atoms.set_distance(idxO, idxH1, d_oh, fix=0, mic=True)
            atoms.set_distance(idxO, idxH2, d_oh, fix=0, mic=True)
            atoms.set_distance(idxH1, idxH2, d_hh, fix=0.5, mic=True)
            cur_d_oh1, cur_d_oh2 = atoms.get_distances(idxO, [idxH1, idxH2])
            cur_d_hh = atoms.get_distance(idxH1, idxH2)

    return atoms
