# ASE Helper Tools

## Introduction

A package of `python3` modules that work in tandem with [Atomic Simulation
Environment (ASE)](https://wiki.fysik.dtu.dk/ase/index.html).
These modules are too niche and/or too specifically
tailored to a certain problem to be worth being implemented as official ASE
functionality.

The modules in this repository are provided without any warranty nor guarantee
for operativeness / compatibility with whatever the most recent ASE release
version is. If you find a bug / issue, feel free to report to the email given
below or open an [issue](https://gitlab.com/bjk24/asetools/-/issues).

Cheers and happy simulating,<br/>
Björn

```
Björn Kirchhoff, Ph.D.
Institute of Electrochemistry
Ulm University
Albert-Einstein-Allee 47
89134 Ulm, Germany
bjoern.kirchhoff [at] protonmail [dot] com
```

## Documentation

A Jupyter book based online documentation web page is available at
[https://bjk24.gitlab.io/asetools](https://bjk24.gitlab.io/asetools).
All mandatory and optional function parameters are documented here.

Some Jupyter notebooks with examples are provided to showcase
the utilities in this package.

## Installation and Usage

We assume that you already have a working installation of
[ASE](https://wiki.fysik.dtu.dk/ase/install.html) on your system,
including its dependencies (notably `numpy` and `scipy`).

1) Clone the repository to your local machine or HPC user:

```git clone https://gitlab.com/bjk24/asetools.git```

2) Add the location to your $PYTHONPATH:

```export PYTHONPATH=$PYTHONPATH:[path-to-asetools]```

3) Import the functions you need in your `python3` ASE script:

```from asetools.watertools import rearrange_atoms```

## License
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

If you use code from this repository to generate data for a publication,
please link to this repository to give attribution.


## Logo Credit Attribution
<a href="https://www.flaticon.com/free-icons/business-and-finance" title="business and finance icons">Business and finance icons created by Freepik - Flaticon</a>