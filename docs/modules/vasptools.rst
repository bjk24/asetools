vasptools
=========

clean_poscar
------------
.. autofunction:: asetools.vasptools.clean_poscar

visualize_bader
---------------
.. autofunction:: asetools.vasptools.visualize_bader