systemtools
===========

cd
--
.. autofunction:: asetools.systemtools.cd

read_bgf
--------
.. autofunction:: asetools.systemtools.read_bgf

.. warning::
    In its current implementation, this bgf reader can only read
    orthorhombic cells, *i.e.* only 90 ° angles. For non-orthorhombic
    cells, the `.bgf` file format requires a coordinate system transformation
    which is currently not implemented. If you need the ability to convert
    non-orthorhombic cells, check out
    [John Kitchin's implementation](https://github.com/jkitchin/python-reaxff)
    which is more feature-complete.